const cors = require("cors");
const express = require("express");
const formdata = require("express-form-data");
const { env } = require("./configs");
const routes = require("./routes");
const { toBeDispatched, shortenedLinks } = require("./configs/store");

const app = express();
const port = 8000;

const http = require("http").Server(app);
const io = require("socket.io")(http);
global.io = io;

app.use(formdata.parse());
app.use(express.json({ limit: "100mb", type: "application/json" }));
app.use(express.urlencoded({ limit: "100mb", extended: true }));
app.use(cors());

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.use("", routes);

io.on("connection", function (socket) {
  socket.on("acknowledge", function (url) {
    const key = url.replace("http://localhost:8000/", "");

    delete toBeDispatched[key];
  });

  socket.on("disconnect", function () {});
});

if (require.main) {
  http.listen(port, () => {
    console.log(
      `MightyByte Backend Challenge is running on http://localhost:${port} (${env.env})`
    );
  });

  setInterval(() => {
    for (const key in toBeDispatched) {
      if (Object.hasOwnProperty.call(toBeDispatched, key)) {
        const element = toBeDispatched[key];
        if (element) {
          const payload = { shortenedURL: `http://localhost:8000/${key}` };

          global.io.emit("send", payload);
        }
      }
    }
  }, 10000);
}
