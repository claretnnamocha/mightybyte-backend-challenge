const { config } = require("dotenv");
const Joi = require("joi");

config();

const schema = Joi.object({
  NODE_ENV: Joi.string()
    .valid("development", "production", "staging")
    .default("development"),
})
  .unknown()
  .required();

const { error, value } = schema.validate(process.env);

if (error) throw error;

const env = value.NODE_ENV;
const port = value.PORT;
const debug = ["development", "staging"].includes(env) || value.DEBUG;

module.exports = { env, port, debug };
