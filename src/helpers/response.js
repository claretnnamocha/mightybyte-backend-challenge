const status = require("http-status");

module.exports.response = async (res, data, code = status.OK) => {
  res.status(code).send({
    ...data,
    timestamp: `${new Date().toUTCString()}`,
  });
};
