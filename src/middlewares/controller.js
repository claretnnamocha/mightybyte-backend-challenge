const { debug } = require("../configs/env");
const { response } = require("../helpers");
const status = require("http-status");

module.exports.controller = (fn) => async (req, res) => {
  try {
    const data = await fn(req.form);

    if (data.code) {
      const { code, payload } = data;
      return response(res, payload, code, debug);
    }

    const code = data.status ? status.OK : status.BAD_REQUEST;
    return response(res, data, code, debug);
  } catch (error) {
    return response(
      res,
      { status: false, message: "Internal server error", error },
      status.INTERNAL_SERVER_ERROR,
      debug
    );
  }
};
