const Joi = require("joi");
const { response } = require("../helpers");
const status = require("http-status");

module.exports.validate = (obj) => (req, res, next) => {
  const schema = Joi.object().keys(obj).required().unknown(false);
  const payload = req.method === "GET" ? req.query : req.body;
  const { params } = req;
  const value = { ...params, ...payload };

  const duplicateFields = Object.keys(payload).filter(
    {}.hasOwnProperty.bind(params)
  );

  if (duplicateFields.length) {
    return response(
      res,
      {
        status: false,
        message: `Duplicate  fields '${duplicateFields.join(
          ", "
        )}' present in url params and ${
          req.method === "GET" ? "query" : "body"
        }`,
      },
      status.CONFLICT
    );
  }

  const { error, value: vars } = schema.validate(value);

  if (error)
    return response(
      res,
      { status: false, message: error.message },
      status.UNPROCESSABLE_ENTITY
    );

  req.form = req.form || {};
  req.form = { ...req.form, ...vars };

  return next();
};
