const { controller } = require("./controller");
const { validate } = require("./validate");

module.exports = { controller, validate };
