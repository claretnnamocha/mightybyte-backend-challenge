const { Router } = require("express");
const { response } = require("./helpers");
const api = require("./modules/api/routes");
const status = require("http-status");

const routes = Router();

routes.use("", api);

routes.use((_, res) => {
  response(
    res,
    { status: false, message: "Route not found" },
    status.NOT_FOUND
  );
});

module.exports = routes;
