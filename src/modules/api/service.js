const status = require("http-status");
const randomstring = require("randomstring");
const { shortenedLinks, toBeDispatched } = require("../../configs/store");

/**
 * Creates a link
 */
module.exports.createLink = async (params) => {
  try {
    let key = randomstring.generate({
      length: 10,
      charset: "alphabetic",
    });

    while (shortenedLinks[key]) {
      key = randomstring.generate({
        length: 10,
        charset: "alphabetic",
      });
    }

    const payload = { shortenedURL: `http://localhost:8000/${key}` };

    global.io.emit("send", payload);

    shortenedLinks[key.toLowerCase()] = params.url;
    toBeDispatched[key.toLowerCase()] = true;

    return {
      code: status.CREATED,
      payload: { success: true, message: "Link created" },
    };
  } catch (error) {
    return {
      payload: {
        success: false,
        message: "Error trying to create a link",
      },
      code: status.INTERNAL_SERVER_ERROR,
    };
  }
};

/**
 * Gets a short link
 */
module.exports.getShortLink = async (params) => {
  try {
    let url = shortenedLinks[params.key.toLowerCase()];

    if (!url) {
      return {
        code: status.NOT_FOUND,
        payload: { success: false, message: "Shortened link Not Found" },
      };
    }

    return {
      code: status.OK,
      payload: { success: true, message: "Shortened link", data: { url } },
    };
  } catch (error) {
    return {
      payload: {
        success: false,
        message: "Error trying to get short link",
      },
      code: status.INTERNAL_SERVER_ERROR,
    };
  }
};
