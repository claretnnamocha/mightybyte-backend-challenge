const { Router } = require("express");
const { controller, validate } = require("../../middlewares");
const service = require("./service");
const validator = require("./validators");

const routes = Router();

routes.post(
  "/url",
  validate(validator.createLink),
  controller(service.createLink)
);

routes.get(
  "/:key",
  validate(validator.getShortLink),
  controller(service.getShortLink)
);

module.exports = routes;
