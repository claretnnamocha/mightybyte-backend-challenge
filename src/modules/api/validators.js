const Joi = require("joi");

module.exports.createLink = {
  url: Joi.string().uri().lowercase().required(),
};

module.exports.getShortLink = {
  key: Joi.string().required(),
};
